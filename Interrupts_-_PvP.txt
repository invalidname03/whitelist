206931--Blooddrinker
198013--Eye Beam
205630--Illidan's Grasp
339--Entangling Roots
33786--Cyclone (Feral/Restoration)
209753--Cyclone (Balance)
194153--Lunar Strike
274281--New Moon
289022--Nourish
8936--Regrowth
50769--Revive
5176--Solar Wrath (Restoration)
190984--Solar Wrath (Balance)
202347--Stellar Flare
740--Tranquility
212640--Mending Bandage
982--Revive Pet
30451--Arcane Blast
5143--Arcane Missiles
190356--Blizzard
257537--Ebonbolt
12051--Evocation
133--Fireball
44614--Flurry
116--Frostbolt
199786--199786
203286--Greater Pyroblast
28271--Polymorph Turtle
28272--Polymorph Pig
61305--Polymorph Black Cat
61721--Polymorph Rabbit
161372--Polymorph Peacock
277787--Polymorph Direhorn
277792--Polymorph Bumblebee
118--Polymorph Sheep
61780--Polymorph Turkey
126819--Polymorph Porcupine
161353--Polymorph Polar Bear Cub
161354--Polymorph Monkey
161355--Polymorph Penguin
11366--Pyroblast
205021--Ray of Frost
113724--Ring of Frost
116011--Rune of Power
2948--Scorch
123986--Chi Burst
124682--Enveloping Mist
191837--Essence of the Mists
115178--Resuscitate
115175--Soothing Mist
209525--Soothing Mist (PvP Talent)
227344--Surging Mist
116670--Vivify
19750--Flash of Light
82326--Holy Light
7328--Redemption
20066--Repentance
32546--Binding Heal
263346--Dark Void
64843--Divine Hymn
2061--Flash Heal
289666--Greater Heal
2060--Heal
289657--Holy Word: Concentration
32375--Mass Dispel
8092--Mind Blast
605--Mind Control
15407--Mind Flay
48045--Mind Sear
596--Prayer of Healing
33076--Prayer of Mending
2006--Resurrection
214621--Schism
186263--Shadow Mend
205351--Shadow Word: Void
34914--Vampiric Touch
228260--Void Eruption
263165--Void Torrent
2008--Ancestral Spirit
1064--Chain Heal
421--Chain Lightning(Restoration)
188443--Chain Lightning(Elemental)
117014--Elemental Blast
73920--Healing Rain
8004--Healing Surge(Elemental, Restoration)
188070--Healing Surge(Enhancement)
77472--Healing Wave
51514--Hex Frog
210873--Hex Compy
211004--Hex Spider
211010--Hex Snake
211015--Hex Cockroach
269352--Hex Skeletal Hatchling
277778--Hex Zandalari Tendonripper
277784--Hex Wicker Mongrel
210714--Icefury
403--Lightning Bolt(Restoration)
187837--Lightning Bolt(Enhancement)
188196--Lightning Bolt(Elemental)
305483--Lightning Lasso
710--Banish
152108--Cataclysm
116858--Chaos Bolt
111771--Demonic Gateway
234153--Drain Life
198590--Drain Soul
5782--Fear
105174--Hand of Gul'dan
48181--Haunt
755--Health Funnel
348--Immolate
29722--Incinerate
27243--Seed of Corruption
686--Shadow Bolt(Demonology, Destruction)
232670--Shadow Bolt(Affliction)
30283--Shadowfury
6353--Soul Fire
265187--Summon Demonic Tyrant
30146--Summon Felguard
691--Summon Felhunter
688--Summon Imp
712--Summon Succubus
264119--Summon Vilefiend
697--Summon Voidwalker
30108--Unstable Affliction